
## Docker
Docker image for [X-BOT](https://github.com/X-Newbie/X-BOT)

## Status
<p align="center">
 <a href="https://github.com/X-Newbie/Docker/actions?query=Docker+build"> <img src="https://img.shields.io/github/workflow/status/X-Newbie/Docker/Docker%20Build/master?color=brightgreen&label=Docker%20build&logo=github%20actions&logoColor=brightgreen&style=for-the-badge" /></a>
 <a href="https://hub.docker.com/r/xnewbie/xbot/tags"> <img src="https://img.shields.io/docker/v/xnewbie/xbot/groovy?label=docker%20version&logo=docker&style=for-the-badge" /></a>
</p>

